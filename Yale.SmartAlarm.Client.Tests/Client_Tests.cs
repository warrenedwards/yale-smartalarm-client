using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.Extensions.Options;
using Moq;
using Moq.Protected;
using System;
using System.Net;
using System.Net.Http;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Yale.SmartAlarm.Client.Tests
{
    public class Client_Tests
    {
        private Mock<HttpMessageHandler> CreateHttpMessageHandlerMock(HttpResponseMessage messageToReturn)
        {
            if (messageToReturn == null) throw new ArgumentNullException(nameof(messageToReturn));

            // Mock the HttpClient to return a 404.
            var httpMessageHandlerMock = new Mock<HttpMessageHandler>();
            httpMessageHandlerMock
                .Protected()
                .Setup<Task<HttpResponseMessage>>(
                    "SendAsync",
                    ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(messageToReturn);

            return httpMessageHandlerMock;
        }

        private HttpClient CreateHttpClient(HttpResponseMessage messageToReturn)
        {
            if (messageToReturn == null) throw new ArgumentNullException(nameof(messageToReturn));

            // Mock the HttpClient to return provided message.
            var httpMessageHandlerMock = this.CreateHttpMessageHandlerMock(messageToReturn);
            var httpClient = new HttpClient(httpMessageHandlerMock.Object);

            return httpClient;
        }

        [Fact]
        public void Client_GetBearerFails()
        {
            var settings = new YaleSmartAlarmClientSettings { Username = "invalid@username.com", Password = "MyPassword1" };

            var nullLogger = NullLogger<YaleSmartAlarmClient>.Instance;

            // Mock the http response.
            var http404Response = new HttpResponseMessage(HttpStatusCode.NotFound)
            {
                ReasonPhrase = "Something went wrong"
            };

            // Mock the HttpClient to return a 404.
            var httpClient = this.CreateHttpClient(http404Response);

            var client = new YaleSmartAlarmClient(settings, httpClient, nullLogger);
            Assert.ThrowsAsync<HttpRequestException>(client.GetBearerToken);
        }

        [Fact]
        public async void Client_GetBearerOK()
        {
            var settings = new YaleSmartAlarmClientSettings { Username = "invalid@username.com", Password = "MyPassword1" };

            var nullLogger = NullLogger<YaleSmartAlarmClient>.Instance;

            // Mock the http response.
            var http200Message = new HttpResponseMessage(HttpStatusCode.OK);

            var expectedResponse = new
            {
                refresh_token = "UYhasdv0cJPBUNz2Dkg7hLHQordDy0hUpy",
                token_type = "Bearer",
                expires_in = 259200,
                scope = "groups basic_profile write google_profile read",
                access_token = "lMtULV1asdOi44mQ3Lv1nC0Ubnepuk"
            };

            var expectedResponseJson = JsonSerializer.Serialize(expectedResponse);

            http200Message.Content = new StringContent(expectedResponseJson);

            // Mock the HttpClient to return a 404.
            var httpClient = this.CreateHttpClient(http200Message);

            var client = new YaleSmartAlarmClient(settings, httpClient, nullLogger);
            var bearerToken = await client.GetBearerToken();

            Assert.Equal(expectedResponse.refresh_token, bearerToken.RefreshToken);
            Assert.Equal(expectedResponse.token_type, bearerToken.TokenType);
            Assert.Equal(expectedResponse.expires_in, bearerToken.ExpiresIn);
            Assert.Equal(expectedResponse.scope, bearerToken.Scope);
            Assert.Equal(expectedResponse.access_token, bearerToken.AccessToken);
        }
    }
}