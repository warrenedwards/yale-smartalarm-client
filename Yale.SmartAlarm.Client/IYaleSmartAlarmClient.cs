﻿using Yale.SmartAlarm.Client.Entities;
using System.Threading.Tasks;

namespace Yale.SmartAlarm.Client
{
    public interface IYaleSmartAlarmClient
    {
        Task<YaleDeviceResult> GetDevicesAsync(YaleDeviceFilter filter);

        Task<YaleDeviceResult> GetDevicesAsync();

        Task<YaleStatusResult> GetStatusAsync();
    }
}