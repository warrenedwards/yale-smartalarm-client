﻿using Yale.SmartAlarm.Client.Entities;

namespace Yale.SmartAlarm.Client
{
    public static class YaleEnumExtensions
    {
        public static string ToYaleEnumString<T>(this T val)
        {
            var attributes = (YaleEnumMember[])val
               .GetType()
               .GetField(val.ToString())
               .GetCustomAttributes(typeof(YaleEnumMember), false);
            return attributes.Length > 0 ? attributes[0].Value : string.Empty;
        }
    }
}