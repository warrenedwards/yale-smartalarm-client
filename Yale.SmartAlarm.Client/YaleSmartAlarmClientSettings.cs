﻿namespace Yale.SmartAlarm.Client
{
    public class YaleSmartAlarmClientSettings
    {
        public string Username { get; set; }

        public string Password { get; set; }
    }
}