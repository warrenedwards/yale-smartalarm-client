﻿using Yale.SmartAlarm.Client.Entities;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Threading.Tasks;

namespace Yale.SmartAlarm.Client
{
    public class YaleSmartAlarmClient : IYaleSmartAlarmClient
    {
        private static class Constants
        {
            public const string ApiAuthUrlPath = "yapi/o/token/";
            public const string ApiBaseUrl = "https://mob.yalehomesystem.co.uk";
            public const string ApiGetStatusPath = "yapi/api/panel/mode/";
            public const string ApiGetDevicesPath = "yapi/api/panel/device/";
            public const string ApiInitialAuthToken = "VnVWWDZYVjlXSUNzVHJhcUVpdVNCUHBwZ3ZPakxUeXNsRU1LUHBjdTpkd3RPbE15WEtENUJ5ZW1GWHV0am55eGhrc0U3V0ZFY2p0dFcyOXRaSWNuWHlSWHFsWVBEZ1BSZE1xczF4R3VwVTlxa1o4UE5ubGlQanY5Z2hBZFFtMHpsM0h4V3dlS0ZBcGZzakpMcW1GMm1HR1lXRlpad01MRkw3MGR0bmNndQ==";
        }

        private YaleBearerToken _bearerToken;

        private readonly HttpClient _httpClient;
        private readonly ILogger<YaleSmartAlarmClient> _logger;
        private readonly YaleSmartAlarmClientSettings _yaleSmartAlarmClientSettings;

        public YaleSmartAlarmClient(
            YaleSmartAlarmClientSettings yaleSmartAlarmClientSettings,
            HttpClient httpClient,
            ILogger<YaleSmartAlarmClient> logger)
        {
            this._yaleSmartAlarmClientSettings = yaleSmartAlarmClientSettings ?? throw new ArgumentException(nameof(yaleSmartAlarmClientSettings));

            if (string.IsNullOrEmpty(yaleSmartAlarmClientSettings.Password)) throw new ArgumentException("Password is empty", nameof(yaleSmartAlarmClientSettings));
            if (string.IsNullOrEmpty(yaleSmartAlarmClientSettings.Username)) throw new ArgumentException("Username is empty", nameof(yaleSmartAlarmClientSettings));

            this._httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
            this._httpClient.BaseAddress = new Uri(Constants.ApiBaseUrl);

            this._logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        private HttpClient GetAuthorisedClient()
        {
            this._httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", this.BearerToken.AccessToken);
            return this._httpClient;
        }

        private YaleBearerToken BearerToken
        {
            get
            {
                return this._bearerToken ?? (this._bearerToken = GetBearerToken().Result);
            }
        }

        public async Task<YaleBearerToken> GetBearerToken()
        {
            var collection = new Dictionary<string, string>
            {
                { "grant_type", "password" },
                { "username", this._yaleSmartAlarmClientSettings.Username },
                { "password", this._yaleSmartAlarmClientSettings.Password }
            };

            var content = new FormUrlEncodedContent(collection);

            this._httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Constants.ApiInitialAuthToken);

            var response = await this._httpClient.PostAsync(Constants.ApiAuthUrlPath, content);

            if (!response.IsSuccessStatusCode)
            {
                this._logger.LogError(response.ReasonPhrase);
                throw new HttpRequestException(response.ReasonPhrase);
            }

            var responseContnet = await response.Content.ReadAsStringAsync();

            var bearerToken = JsonSerializer.Deserialize<YaleBearerToken>(responseContnet);

            this._bearerToken = bearerToken;

            return this._bearerToken;
        }

        public async Task<YaleDeviceResult> GetDevicesAsync(YaleDeviceFilter filter)
        {
            if (filter == null) throw new ArgumentNullException(nameof(filter));

            var client = this.GetAuthorisedClient();
            var response = await client.GetAsync(Constants.ApiGetDevicesPath);
            var json = await response.Content.ReadAsStringAsync();
            var yaleDeviceResult = JsonSerializer.Deserialize<YaleDeviceResult>(json);

            var filteredYaleDevices = yaleDeviceResult.Data
                .Where(x => string.IsNullOrWhiteSpace(filter.DeviceId) || x.DeviceId == filter.DeviceId)
                .Where(x => filter.DeviceType == null || x.Type == filter.DeviceType.Value)
                .Where(x => string.IsNullOrWhiteSpace(filter.Name) || x.Name.Contains(filter.Name));

            // Replace the devices collection with the filtered collection.
            yaleDeviceResult.Data = filteredYaleDevices;

            return yaleDeviceResult;
        }

        public async Task<YaleStatusResult> GetStatusAsync()
        {
            var client = this.GetAuthorisedClient();
            var response = await client.GetAsync(Constants.ApiGetStatusPath);

            var content = await response.Content.ReadAsStringAsync();
            var result = JsonSerializer.Deserialize<YaleStatusResult>(content);

            return result;
        }

        public async Task<YaleDeviceResult> GetDevicesAsync()
        {
            return await this.GetDevicesAsync(new YaleDeviceFilter());
        }
    }
}