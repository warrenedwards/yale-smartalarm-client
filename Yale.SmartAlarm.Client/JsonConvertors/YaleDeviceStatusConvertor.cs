﻿using System;
using System.Text.Json.Serialization;
using System.Text.Json;
using Yale.SmartAlarm.Client.Entities;

namespace Yale.SmartAlarm.Client.JsonConvertors
{
    public class YaleDeviceStatusConvertor : JsonConverter<YaleDeviceStatus>
    {
        public override YaleDeviceStatus Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            var from = reader.GetString();

            if (from == YaleDeviceStatus.Closed.ToYaleEnumString())
            {
                return YaleDeviceStatus.Closed; ;
            }
            else if (from == YaleDeviceStatus.Open.ToYaleEnumString())
            {
                return YaleDeviceStatus.Open;
            }
            else
            {
                return YaleDeviceStatus.Unknown;
            }
        }

        public override void Write(Utf8JsonWriter writer, YaleDeviceStatus value, JsonSerializerOptions options)
        {
            writer.WriteStringValue(value.ToString());
        }
    }
}