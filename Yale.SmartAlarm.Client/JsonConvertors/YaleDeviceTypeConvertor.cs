﻿using System;
using System.Text.Json.Serialization;
using System.Text.Json;
using Yale.SmartAlarm.Client.Entities;

namespace Yale.SmartAlarm.Client.JsonConvertors
{
    public class YaleDeviceTypeConvertor : JsonConverter<YaleDeviceType>
    {
        public override YaleDeviceType Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            var from = reader.GetString();

            if (from == YaleDeviceType.DoorContact.ToYaleEnumString())
            {
                return YaleDeviceType.DoorContact;
            }
            else if (from == YaleDeviceType.ExternalSiren.ToYaleEnumString())
            {
                return YaleDeviceType.ExternalSiren;
            }
            else if (from == YaleDeviceType.Keypad.ToYaleEnumString())
            {
                return YaleDeviceType.Keypad;
            }
            else if (from == YaleDeviceType.PanicButton.ToYaleEnumString())
            {
                return YaleDeviceType.PanicButton;
            }
            else if (from == YaleDeviceType.PIR.ToYaleEnumString())
            {
                return YaleDeviceType.PIR;
            }
            else
            {
                return YaleDeviceType.Unknown;
            }
        }
        public override void Write(Utf8JsonWriter writer, YaleDeviceType value, JsonSerializerOptions options)
        {
            writer.WriteStringValue(value.ToString());
        }
    }
}