﻿using System;
using System.Text.Json.Serialization;
using System.Text.Json;
using System.Net.NetworkInformation;

namespace Yale.SmartAlarm.Client.JsonConvertors
{
    public class YalePhysicalAddressConvertor : JsonConverter<PhysicalAddress>
    {
        public override PhysicalAddress Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            PhysicalAddress result = null;

            var value = reader.GetString();

            if (string.IsNullOrEmpty(value)) return result;

            var standardisedMacAddress = value.ToUpper().Replace(":", "");

            try
            {
                result = PhysicalAddress.Parse(standardisedMacAddress);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public override void Write(Utf8JsonWriter writer, PhysicalAddress value, JsonSerializerOptions options)
        {
            writer.WriteStringValue(value.ToString());
        }
    }
}