﻿using Yale.SmartAlarm.Client.Entities;

namespace Yale.SmartAlarm.Client
{
    public class YaleDeviceFilter
    {
        public string DeviceId { get; set; }

        public YaleDeviceType? DeviceType { get; set; }

        public string Name { get; set; }
    }
}