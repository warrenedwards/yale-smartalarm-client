﻿using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;

namespace Yale.SmartAlarm.Client.Entities
{
    public class YaleStatusResult
    {
        [JsonPropertyName("result")]
        public bool Result { get; set; }

        [JsonPropertyName("code")]
        public string Code { get; set; }

        [JsonPropertyName("message")]
        public string Message { get; set; }

        [JsonPropertyName("token")]
        public string Token { get; set; }

        /// <summary>
        /// Gets the area.
        /// </summary>
        /// <remarks>Helper method which extracts Area value from the Data collection.</remarks>
        public string Area
        {
            get
            {
                var area = this.Data.Where(x => x.ContainsKey("area"))
                    .Select(x => x["area"])
                    .FirstOrDefault();

                return area;
            }
        }

        /// <summary>
        /// Gets the Mode.
        /// </summary>
        /// <remarks>Helper method which extracts Mode value from the Data collection.</remarks>
        public YaleDeviceMode Mode
        {
            get
            {
                var mode = this.Data.Where(x => x.ContainsKey("mode"))
                    .Select(x => x["mode"])
                    .FirstOrDefault();

                switch (mode)
                {
                    case "disarm": return YaleDeviceMode.Disarm;
                    case "home": return YaleDeviceMode.Home;
                    case "part": return YaleDeviceMode.Part;
                    default: return YaleDeviceMode.Unknown;
                }
            }
        }

        [JsonPropertyName("data")]
        public Dictionary<string, string>[] Data { get; set; }

        [JsonPropertyName("time")]
        public string Time { get; set; }
    }
}