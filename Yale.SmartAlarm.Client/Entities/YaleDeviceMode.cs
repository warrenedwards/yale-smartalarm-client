﻿namespace Yale.SmartAlarm.Client.Entities
{
    public enum YaleDeviceMode
    {
        Unknown = 0,
        Disarm = 1,
        Home = 2,
        Part = 3
    }
}