﻿using System;

namespace Yale.SmartAlarm.Client.Entities
{
    public class YaleEnumMember : Attribute
    {
        public string Value { get; set; }
    }
}