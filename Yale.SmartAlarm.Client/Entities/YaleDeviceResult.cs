﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text.Json.Serialization;

namespace Yale.SmartAlarm.Client.Entities
{
    public class YaleDeviceResult
    {
        [JsonPropertyName("message")]
        public string Message { get; set; }

        [JsonPropertyName("data")]
        public IEnumerable<YaleDevice> Data { get; set; } = new Collection<YaleDevice>();
    }
}