﻿namespace Yale.SmartAlarm.Client.Entities
{
    public enum YaleDeviceType
    {
        [YaleEnumMember(Value = "")]
        Unknown = 0,
        
        [YaleEnumMember(Value = "device_type.door_contact")]
        DoorContact = 1,

        [YaleEnumMember(Value = "device_type.bx")]
        ExternalSiren = 2,

        [YaleEnumMember(Value = "device_type.keypad")]
        Keypad = 3,

        [YaleEnumMember(Value = "device_type.panic_button")]
        PanicButton = 4,

        [YaleEnumMember(Value = "device_type.pir")]
        PIR = 5
    }
}