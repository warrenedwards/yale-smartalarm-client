﻿using Yale.SmartAlarm.Client.JsonConvertors;
using System.Net.NetworkInformation;
using System.Text.Json.Serialization;

namespace Yale.SmartAlarm.Client.Entities
{
    public class YaleDevice
    {
        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("type")]
        [JsonConverter(typeof(YaleDeviceTypeConvertor))]
        public YaleDeviceType Type { get; set; }

        [JsonPropertyName("mac")]
        [JsonConverter(typeof(YalePhysicalAddressConvertor))]
        public PhysicalAddress Mac { get; set; }

        [JsonPropertyName("bypass")]
        public string Bypass { get; set; }

        [JsonPropertyName("status1")]
        [JsonConverter(typeof(YaleDeviceStatusConvertor))]
        public YaleDeviceStatus Status1 { get; set; }

        [JsonPropertyName("device_id")]
        public string DeviceId { get; set; }
    }
}