﻿namespace Yale.SmartAlarm.Client.Entities
{
    public enum YaleDeviceStatus
    {
        [YaleEnumMember(Value = "")]
        Unknown = 0,

        [YaleEnumMember(Value = "device_status.dc_open")]
        Open = 1,

        [YaleEnumMember(Value = "device_status.dc_close")]
        Closed = 2
    }
}